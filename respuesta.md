## Ejercicio 3. Análisis general

* ¿Cuántos paquetes componen la captura?
    * La captura está compuesta por 1090 paquetes.
* ¿Cuánto tiempo dura la captura?
    * 14.491079516 segundos.
* ¿Qué IP tiene la máquina donde se ha efectuado la captura? ¿Se trata de una IP pública o de una IP privada? ¿Por qué lo sabes?
    * La IP de la máquina es 192.168.1.34. Es una IP privada, ya que al comenzar con 192.168. esto nos indica que es una IP privada de clase C.

Antes de analizar las tramas, mira las estadísticas generales que aparecen en el menú de `Statistics`. En el apartado de jerarquía de protocolos (`Protocol Hierarchy`) se puede ver el número de paquetes y el tráfico correspondiente a los distintos protocolos.

* ¿Cuáles son los tres principales protocolos de nivel de aplicación por número de paquetes?
    * RTP representa el 96.2% (1049 paquetes), SIP el 0.8% (9 paquetes) y STUN el 0.2% (2 paquetes).
* ¿Qué otros protocolos podemos ver en la jerarquía de protocolos?
    * Vemos también el protocolo ICMP, que representa el 0.3% de los paquetes de la captura.
* ¿Qué protocolo de nivel de aplicación presenta más tráfico, en bytes por segundo? ¿Cuánto es ese tráfico?
    * El protocolo que presenta más tráfico es el RTP. En total se han enviado 180.428 bytes a 99.000 Bits/s.

Observa por encima el flujo de tramas en el menú de `Statistics` en `IO Graphs`. La captura que estamos viendo incluye desde la inicialización (registro) de la aplicación hasta su finalización, incluyendo una llamada. 

* Filtra por `sip` para conocer cuándo se envían paquetes SIP. ¿En qué segundos tienen lugar esos envíos?
    * En el 0.00, 0.07, 0.09, 0.14, 3.87, 3.92, 3.94, 14.44 y 14.49.
* Y los paquetes con RTP, ¿cuándo se empiezan a enviar?
    * A partir del paquete 12, 3.97 segundos.
* Los paquetes RTP, ¿cuándo se dejan de enviar?
    * En el paquete 1088, 14.48 segundos.
* Los paquetes RTP, ¿cada cuánto se envían?
    * Se envía un paquete RTP cada 0.01 segundos.


## Ejercicio 4. Primeras tramas

Analiza ahora las dos primeras tramas de la captura. Cuando se pregunta por máquinas, llama "Linphone" a la máquina donde está funcionando el cliente SIP, y "Servidor" a la máquina que proporciona el servicio SIP.

* ¿De qué protocolo de nivel de aplicación son?
  * Protocolo SIP
* ¿Cuál es la dirección IP de la máquina "Linphone"?
  * 192.168.1.34
* ¿Cuál es la dirección IP de la máquina "Servidor"?
  * 212.79.111.155
* ¿En qué máquina está el UA (user agent)?
  * Linphone (192.168.1.34)
* ¿Entre qué máquinas se envía cada trama?
  * La primera trama se envía desde Linphone hasta el Servidor y la segunda desde el Servidor hasta Linphone.
* ¿Que ha ocurrido para que se envíe la primera trama?
  * Linphone envía una petición de REGISTER al servidor.
* ¿Qué ha ocurrido para que se envíe la segunda trama?
  * El servidor contesta al cliente denegándole la petición.

Ahora, veamos las dos tramas siguientes.

* ¿De qué protocolo de nivel de aplicación son?
  * Protocolo SIP
* ¿Entre qué máquinas se envía cada trama?
  * La 3ª se envía desde el cliente hacia el servidor y la 4ª se envía desde el servidor hacia el cliente.
* ¿Que ha ocurrido para que se envíe la primera de ellas (tercera trama en la captura)?
  * El cliente vuelve a enviar una petición REGISTER al servidor.
* ¿Qué ha ocurrido para que se envíe la segunda de ellas (cuarta trama en la captura)?
  * El servidor contesta a la petición, esta vez, aceptándola.

Ahora, las tramas 5 y 6.

* ¿De qué protocolo de nivel de aplicación son?
  * Protocolo SIP
* ¿Entre qué máquinas se envía cada trama?
  * La 5ª se envía desde el cliente hacia el servidor y la 6ª se envía desde el servidor hacia el cliente.
* ¿Que ha ocurrido para que se envíe la primera de ellas (quinta trama en la captura)?
  * El cliente envía una petición INVITE al servidor con el objetivo de realizar el intercambio por RTP.
* ¿Qué ha ocurrido para que se envíe la segunda de ellas (sexta trama en la captura)?
  * El servidor contesta con OK al cliente.
* ¿Qué se indica (en líneas generales) en la parte SDP de cada una de ellas?
  * En el paquete 5 se indica el usuario con la IP, información de la conexión, tiempo, sesión y los atributos de media, es decir, especificaciones del audio.
  * En la 6 se indica la IP del servidor, el ID de la sesión, información de tiempo y de la conexión y el Media Description.

Después de la trama 6, busca la primera trama SIP.

* ¿Qué trama es?
  * Es la trama 11.
* ¿De qué máquina a qué máquina va?
  * Del cliente al servidor.
* ¿Para qué sirve?
  * Envía un ACK que sirve como validación para las condiciones propuestas para el intercambio RTP.
* ¿Puedes localizar en ella qué versión de Linphone se está usando?
  * Se está usando Linphone/3.12.0 (belle-sip/1.6.3)


## Ejercicio 5. Tramas especiales

Las tramas 7 y 9 parecen un poco especiales:

* ¿De que protocolos son (indica todos los protocolos relevantes por encima de IP).
  * Protocolo STUN. Por encima está el protocolo UDP.
* ¿De qué máquina a qué máquina van?
  * Del cliente al servidor.
* ¿Para qué crees que sirven?
  * Con ello, el cliente indica al servidor su dirección IP pública y el puerto escogido para la comunicación.


## Ejercicio 6. Registro

Repasemos ahora qué está ocurriendo (en lo que al protocolo SIP se refiere) en las primeras tramas SIP que hemos visto ya:

* ¿Qué dirección IP tiene el servidor que actúa como registrar SIP? ¿Por qué?
  * 212.79.111.155. Es la dirección a la cuál se manda la primera trama.
* ¿A qué puerto (del servidor Registrar) se envían los paquetes SIP?
  * Al puerto 5060.
* ¿Qué método SIP utiliza el UA para registrarse?
  * REGISTER.
* ¿Qué diferencia fundamental ves entre la primera línea del paquete SIP que envía Linphone para registrar a un usuario, y el que hemos estado enviando en la práctica 4?
  * 
* ¿Por qué tenemos dos paquetes `REGISTER` en la traza?
  * Porque el primer register es denegado por el servidor, ya que no tiene la verificación correspondiente para saber si el que envía la petición es en realidad el cliente. Por ello se envía el segundo REGISTER.
* ¿Por qué la cabecera `Contact` es diferente en los paquetes 1 y 3? ¿Cuál de las dos cabeceras es más "correcta", si nuestro interés es que el UA sea localizable?
  * Porque en el primer request, el UA no es localizable. Por tanto, es más correcta la cabecera 'Contact' del paquete 3.
* ¿Qué tiempo de validez se está dando para el registro que pretenden realizar los paquetes 1 y 3?
  * 3600 segundos.

Para responder estas preguntas, puede serte util leer primero [Understanding REGISTER Method](https://blog.wildix.com/understanding-register-method/), además de tener presente lo explicado en clase.

## Ejercicio 7. Indicación de comienzo de conversación

* Además de REGISTER, ¿podrías decir qué instrucciones SIP entiende el UA?
  * INVITE, ACK y BYE.
* ¿En qué paquete el UA indica que quiere comenzar a intercambiar datos de audio con otro? ¿Qué método SIP utiliza para ello?
  * En el paquete 5. INVITE.
* ¿Con qué dirección quiere intercambiar datos de audio el UA?
  * Con el servidor 212.79.111.155
* ¿Qué protocolo (formato) está usando para indicar cómo quiere que sea la conversación?
  * Con el protocolo SDP.
* En la indicación de cómo quiere que sea la conversación, puede verse un campo `m` con un valor que empieza por `audio 7078`. ¿Qué indica el `7078`? ¿Qué relación tiene con el destino de algunos paquetes que veremos más adelante en la trama? ¿Qué paquetes son esos?
  * El puerto de Media. Por tanto ahí se enviarán los datos del audio. Son paquetes de protocolo RTP.
* En la respuesta a esta indicación vemos un campo `m` con un valor que empieza por `audio 27138`. ¿Qué indica el `27138`? ¿Qué relación tiene con el destino de algunos paquetes que veremos más adelante en la trama? ¿Qué paquetes son esos?
  * El puerto de Media. Por tanto ahí se mandarán los datos del audio. Son paquetes de protocolo RTP.
* ¿Para qué sirve el paquete 11 de la trama?
  * Envía un ACK que sirve como validación para las condiciones propuestas para el intercambio RTP.
## Ejercicio 8. Primeros paquetes RTP

Vamos ahora a centrarnos en los paquetes RTP. Empecemos por el paquete 12 de la traza:

* ¿De qué máquina a qué máquina va?
  * Va del cliente al servidor.
* ¿Qué tipo de datos transporta?
  * Transporta datos de audio.
* ¿Qué tamaño tiene?
  * 214 bytes.
* ¿Cuántos bits van en la "carga de pago" (payload)
  * 1280 bits.
* ¿Se utilizan bits de padding?
  * No.
* ¿Cuál es la periodicidad de los paquetes según salen de la máquina origen?
  * 20 ms aproximadamente.
* ¿Cuántos bits/segundo se envían?
  * 99000 bits/segundo.
Y ahora, las mismas preguntas para el paquete 14 de la traza:

* ¿De qué máquina a qué máquina va?
  * Del servidor a la máquina Linphone.
* ¿Qué tipo de datos transporta?
  * Transporta datos de audio.
* ¿Qué tamaño tiene?
  * 214 bytes.
* ¿Cuántos bits van en la "carga de pago" (payload)
  * 1280 bits.
* ¿Se utilizan bits de padding?
  * No.
*¿Cuál es la periodicidad de los paquetes según salen de la máquina origen?
  * 20 ms aproximadamente.
* ¿Cuántos bits/segundo se envían?
  * 99000 bits/segundo.


## Ejercicio 9. Flujos RTP

Vamos a ver más a fondo el intercambio RTP. Busca en el menú `Telephony` la opción `RTP`. Empecemos mirando los flujos RTP.

* ¿Cuántos flujos hay? ¿por qué?
  * 2 flujos, uno cliente-servidor, y otro servidor-cliente.
* ¿Cuántos paquetes se pierden?
  * No se pierde ningún paquete.
* Para el flujo desde LinPhone hacia Servidor: ¿cuál es el valor máximo del delta?
  * 30.834 ms.
* Para el flujo desde Servidor hacia LinPhone: ¿cuál es el valor máximo del delta?
  * 59.586 ms.
* ¿Qué es lo que significa el valor de delta?
  * Tiempo entre cada paquete.
* ¿En qué flujo son mayores los valores de jitter (medio y máximo)?
  * en el flujo LinPhone-servidor.
* ¿Cuáles son esos valores?
  * Max: 8.445, Mean: 4.153.
* ¿Qué significan esos valores?
  * La fluctuación en el retraso de los paquetes.
* Dados los valores de jitter que ves, ¿crees que se podría mantener una conversación de calidad?
  * Sí, la calidad sería excelente, puesto que los valores son muy bajos, por debajo de 10 ms.

Vamos a ver ahora los valores de un paquete concreto, el paquete 17. Vamos a analizarlo en opción `Stream Analysis`:

* ¿Cuánto valen el delta y el jitter para ese paquete?
  * Delta: 20.58 ms, Jitter: 0.10 ms.
* ¿Podemos saber si hay algún paquete de este flujo, anterior a este, que aún no ha llegado?
  * Sí, mediante la secuencia. Han llegado todos los paquetes.
* El "skew" es negativo, ¿qué quiere decir eso?
  * Que ha habido retraso al mandar el paquete.

Si sigues el jitter, ves que en el paquete 78 llega a ser de 5.52:

* ¿A qué se debe esa subida desde el 0.57 que tenía el paquete 53?
  * A que hay mucha fluctuación entre el tiempo al que llegan los paquetes. En el paquete 53, todos los paquetes se enviaban cada 20 ms aproximadamente. Luego, cambia y varía entre 30 ms, 10 ms. Por ello sube el jitter.

En el panel `Stream Analysis` puedes hacer `play` sobre los streams:

* ¿Qué se oye al pulsar `play`?
  * Se oye la canción Happy Together.
* ¿Qué se oye si seleccionas un `Jitter Buffer` de 1, y pulsas `play`?
  * Se oye la canción, pero esta vez parece que se va pausando.
* ¿A qué se debe la diferencia?
  * Esto se debe al retraso en la llegada de paquetes.
  
  
## Ejercicio 10. Llamadas VoIP

Podemos ver ahora la traza, analizándola como una llamada VoIP completa. Para ello, en el menú `Telephony` seleccina el menú `VoIP calls`, y selecciona la llamada hasta que te salga el panel correspondiente:

* ¿Cuánto dura la llamada?
  * Dura algo más de 10 segundos.

Ahora, pulsa sobre `Flow Sequence`:

* Guarda el diagrama en un fichero (formato PNG) con el nombre `diagrama.png`.

* ¿En qué segundo se realiza el `INVITE` que inicia la conversación?
  * En el segundo 3.877.
* ¿En qué segundo se recibe el último OK que marca su final?
  * En el segundo 14.491.
  
Ahora, pulsa sobre `Play Streams`, y volvemos a ver el panel para ver la transmisión de datos de la llamada (protocolo RTP):

* ¿Cuáles son las SSRC que intervienen?
  * La máquina Linphone (0xeeccf15f) y el servidor (0x761f98b2).
* ¿Cuántos paquetes se envían desde LinPhone hasta Servidor?
  * 524 paquetes.
* ¿Cuántos en el sentido contrario?
  * 525 paquetes.
* ¿Cuál es la frecuencia de muestreo del audio?
  * 8000 Hz.
* ¿Qué formato se usa para los paquetes de audio?
  * g711U
  

## Ejercicio 11. Captura de una llamada VoIP

Dirígete a la [web de LinPhone](https://www.linphone.org/freesip/home) con el navegador y créate una cuenta SIP.  Recibirás un correo electrónico de confirmación en la dirección que has indicado al registrarte (mira en tu carpeta de spam si no es así).
  
Lanza LinPhone, y configúralo con los datos de la cuenta que te acabas de crear. Para ello, puedes ir al menú `Ayuda` y seleccionar `Asistente de Configuración de Cuenta`. Al terminar, cierra completamente LinPhone.

* Captura una llamada VoIP con el identificador SIP `sip:music@sip.iptel.org`, de unos 15 segundos de duración. Recuerda que has de comenzar a capturar tramas antes de arrancar LinPhone para ver todo el proceso. Guarda la captura en el fichero `linphone-music.pcapng`. Procura que en la captura haya sólo paquetes entre la máquina donde has hecho la captura y has ejecutado LinPhone, y la máquina o máquinas que han intervenido en los intercambios SIP y RTP con ella.
* ¿Cuántos flujos tiene esta captura?
  * Tiene 2 flujos.
* ¿Cuánto es el valor máximo del delta y los valores medios y máximo del jitter de cada uno de los flujos?
  * Para el flujo servidor - cliente: Max Delta: 25.118, Max Jitter: 1.197 y Mean Jitter: 0.297.
  * Para el flujo cliente - servidor: Max Delta: 41.666, Max Jitter: 10.294 y Mean Jitter: 8.660.
